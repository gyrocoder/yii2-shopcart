<?php
namespace gyrocoder\yii2\shopcart\widgets\status;
use gyrocoder\yii2\shopcart\assets\FrontendAsset;
use gyrocoder\yii2\shopcart\general\CartComponent;
use gyrocoder\yii2\shopcart\general\CartHandler;
use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class CartStatus
 * Отрисовка текущего статуса корзины покупок
 *
 * @package gyrocoder\yii2\shopcart\widgets\status
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
class CartStatus extends Widget
{
    /**
     * @var null|CartComponent
     */
    public  $component = null;
    public  $view = '@vendor/gyrocoder/yii2-shopcart/widgets/status/views/default';

    public function run()
    {
        FrontendAsset::register($this->getView());
        return $this->render($this->view, [
            'cart' => $this->component,
        ]);
    }

    public function init()
    {
        if (!$this->component || !($this->component instanceof CartComponent)) {
            throw new InvalidArgumentException('Cart Component must be instance of CartComponent interface.');
        }
        parent::init();
    }
}