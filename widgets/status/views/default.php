<?php
/**
 * @var $this \yii\web\View
 */
?>
<ul class="shopcart-status-box">
    <li>Количество товаров: <span data-role="cart-total-quantity"><?=$cart->totalQuantity?></span></li>
    <li>Общая сумма:        <span data-role="cart-total-amount"><?=$cart->totalAmount?></span></li>
</ul>
