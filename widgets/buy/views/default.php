<?php
/**
 * @var $this  \yii\web\View
 * @var $model \gyrocoder\yii2\shopcart\general\Product
 * @var $cart  \gyrocoder\yii2\shopcart\component\ShopCart
 * @var $showPrice boolean
 * @var $priceVisible string
 */

$exists = $cart->hasItem($model->source, $model->id);
?>
<div data-role   = "gsc-item"
     data-url    = "<?=$cart->urlBuy?>"
     data-source = "<?=$model->source?>"
     data-id     = "<?=$model->id?>"
     data-min    = "<?=$model->minAvailable?>"
     data-max    = "<?=$model->maxAvailable?>"
>

    <div class="input-group <?=$exists ? 'hidden' : ''?>" data-filter="notincart" data-role="gsc-control-holder" data-action="purchase">
        <?php if ($showPrice): ?>
            <span class="input-group-addon" data-role="gsc-product-price"><?=$priceVisible?></span>
        <?php endif; ?>
        <span  class="input-group-addon" data-role="gsc-product-minus"><i class="fa fa-fw fa-minus"></i></span>
        <input class="form-control text-center" data-role="gsc-product-quantity" value="<?=$exists['quantity']?>" data-value="gsc-<?=$model->source?>-<?=$model->id?>-quantity">
        <span  class="input-group-addon" data-role="gsc-product-plus"><i class="fa fa-fw fa-plus"></i></span>
        <span  class="input-group-addon" data-role="gsc-product-purchase"><i class="fa fa-fw fa-cart-plus"></i>&nbsp;Купить</a></span>
    </div>

    <div class="input-group <?=$exists ? '' : 'hidden'?>" data-filter="incart" data-role="gsc-control-holder" data-action="recalculate">
        <?php if ($showPrice): ?>
            <span class="input-group-addon" data-role="gsc-product-price"><?=$priceVisible?></span>
        <?php endif; ?>
        <span  class="input-group-addon">В корзине:</span>
        <span  class="input-group-addon" data-role="gsc-product-minus"><i class="fa fa-fw fa-minus"></i></span>
        <input class="form-control text-center" data-role="gsc-product-quantity" value="<?=$exists['quantity']?>" data-value="gsc-<?=$model->source?>-<?=$model->id?>-quantity">
        <span  class="input-group-addon" data-role="gsc-product-plus"><i class="fa fa-fw fa-plus"></i></span>
        <span  class="input-group-addon">На сумму: <span data-role="gsc-product-amount" data-value="gsc-<?=$model->source?>-<?=$model->id?>-amount"><?=$exists['amount']?></span> RUB</span>
        <span  class="input-group-addon" data-role="gsc-product-delete"><i class="fa fa-fw fa-trash"></i></span>
    </div>


</div>
