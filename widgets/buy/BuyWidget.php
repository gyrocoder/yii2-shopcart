<?php
namespace gyrocoder\yii2\shopcart\widgets\buy;
use gyrocoder\yii2\shopcart\general\Product;
use yii\base\Widget;
use gyrocoder\yii2\shopcart\general\CartComponent;
use yii\base\InvalidArgumentException;
use gyrocoder\yii2\shopcart\assets\FrontendAsset;

/**
 * Class BuyWidget
 * Кнопка "Купить" с выбором количества
 * @package gyrocoder\yii2\shopcart\widgets\buy
 */
class BuyWidget extends Widget
{
    /**
     * @var null|CartComponent
     */
    public  $component = null;

    /**
     * @var null|Product
     */
    public  $model = null;

    /**
     * Флаг - добавлять ли перед количеством товара планку
     * @var bool
     */
    public  $showPrice = false;

    /**
     * @var string
     */
    public  $view = '@vendor/gyrocoder/yii2-shopcart/widgets/buy/views/default';

    /**
     * @inheritdoc
     */
    public function run()
    {
        FrontendAsset::register($this->getView());
        return $this->render($this->view, [
            'model'     => $this->model,
            'cart'      => $this->component,
            'showPrice' => $this->showPrice,
            'priceVisible' => $this->getPriceVisible()
        ]);
    }

    public function getPriceVisible()
    {
        return 'Цена: ' . $this->model->price . ' RUB';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->component || !($this->component instanceof CartComponent)) {
            throw new InvalidArgumentException('Cart Component must be instance of CartComponent interface.');
        }
        if (!$this->model || !($this->model instanceof Product)) {
            throw new InvalidArgumentException('Model must be instance of Product interface.');
        }
        parent::init();
    }
}