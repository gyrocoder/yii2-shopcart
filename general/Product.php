<?php
namespace gyrocoder\yii2\shopcart\general;

/**
 * Interface Product
 * Должен быть реализован моделями, взаимодействующими с корзиной покупок.
 *
 * @property $id
 * @property $source
 * @property $minAvailable
 * @property $maxAvailable
 * @property $price
 * @property $cartName
 *
 * @package gyrocoder\yii2\shopcart\general
 * @author  Alexey Volkov <webwizardry@hotmail.com>
 */
interface Product
{
    /**
     * Проверка возможности приобретения заданного количества товара
     *
     * @param int $quantity
     * @return mixed
     */
    public function isPurchaseAvailable($quantity = 1);
}