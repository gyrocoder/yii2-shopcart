<?php
namespace gyrocoder\yii2\shopcart\component;
use gyrocoder\yii2\shopcart\general\CartComponent;
use gyrocoder\yii2\shopcart\general\Product;
use yii\base\Component;

class ShopCart extends Component implements CartComponent
{
    public $totalQuantity = 0;
    public $totalAmount   = 0;

    public $urlBuy;
    public $sessionKey = 'gc-shopcart';
    public $models = [];
    public $errors = [];

    private $items;

    /**
     * Добавление товара в корзину
     *
     * @param $from
     * @param $id
     * @param int $quantity
     *
     * @return boolean
     */
    public function buy($from, $id, $quantity = 1)
    {
        if (isset($this->models[$from])) {
            if (isset($this->items[$from][$id])) {
                $this->items[$from][$id]['quantity'] += $quantity;
            } else {
                $this->items[$from][$id]=['quantity' => $quantity];
            }
            $this->recalculate();
            $this->save();
        } else {
            $this->errors[] = 'Invalid model source ' . $from;
        }

        return empty($this->errors);
    }

    public function delete($from, $id)
    {
        $result = false;
        if (isset($this->items[$from][$id])) {
            $result = true;
        }
        unset($this->items[$from][$id]);
        $this->recalculate();
        $this->save();
        return $result;
    }

    public function setQuantity($from, $id, $quantity)
    {
        if (isset($this->models[$from])) {
            if (isset($this->items[$from][$id])) {
                $this->items[$from][$id]['quantity'] = $quantity;
            } else {
                return false;
            }

            $this->recalculate();
            $this->save();
        } else {
            $this->errors[] = 'Invalid model source ' . $from;
        }
    }

    public function hasItem($from, $id)
    {
        if (isset($this->items[$from][$id])) {
            return $this->items[$from][$id];
        } else
        return null;
    }

    /**
     * Актуализация состояния корзины покупок при каждом действии
     */
    private function recalculate()
    {
        $this->totalAmount = 0;
        $this->totalQuantity = 0;

        foreach ($this->items as $from=>$products) {
            $className = $this->models[$from];
            foreach ($products as $id=>$product) {
                $origin = $className::findOne($id);
                /** @var Product $origin */
                $this->items[$from][$id]['comment'] = '';

                if (!isset($this->items[$from][$id]['quantity'])) $this->items[$from][$id]['quantity'] = $origin->minAvailable;
                if ($this->items[$from][$id]['quantity'] < $origin->minAvailable) {
                    $this->items[$from][$id]['quantity'] = $origin->minAvailable;
                    $this->items[$from][$id]['comment']  = 'Установлен минимально возможный размер заказа';
                }
                if ($this->items[$from][$id]['quantity'] > $origin->maxAvailable) {
                    $this->items[$from][$id]['quantity'] = $origin->maxAvailable;
                    $this->items[$from][$id]['comment']  = 'Установлен максимально возможный размер заказа';
                }
                $this->items[$from][$id]['name']   = $origin->cartName;
                $this->items[$from][$id]['price']  = $origin->price;
                $this->items[$from][$id]['amount'] = $this->items[$from][$id]['price'] * $this->items[$from][$id]['quantity'];

                $this->totalQuantity += $this->items[$from][$id]['quantity'];
                $this->totalAmount   += $this->items[$from][$id]['amount'];
            }
        }
    }

    private function save()
    {
        \Yii::$app->session->set($this->sessionKey, $this->items);
    }

    public function init()
    {
        $this->items = \Yii::$app->session->get($this->sessionKey);
        if (!$this->items) {
            $this->items = [];
        }
        $this->recalculate();
        parent::init();
    }
}