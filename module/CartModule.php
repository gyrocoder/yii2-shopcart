<?php
namespace gyrocoder\yii2\shopcart\module;
use gyrocoder\yii2\shopcart\component\ShopCart;
use yii\base\InvalidArgumentException;
use yii\base\Module;


/**
 * Class CartModule
 *
 * @property $cart ShopCart
 * @package gyrocoder\yii2\shopcart\module
 */
class CartModule extends Module
{
    public $componentName = null;

    /**
     * @return ShopCart
     */
    public function getCart()
    {
        $cn = $this->componentName;
        return \Yii::$app->$cn;
    }

    public function init()
    {
        $cn = $this->componentName;
        if (!$cn || !\Yii::$app->$cn) throw new InvalidArgumentException('ShopCart component is not set');
        parent::init();
    }
}