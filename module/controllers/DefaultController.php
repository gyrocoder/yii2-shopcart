<?php
namespace gyrocoder\yii2\shopcart\module\controllers;
use gyrocoder\yii2\shopcart\module\CartModule;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package gyrocoder\yii2\shopcart\module\controllers
 */
class DefaultController extends Controller
{
    /**
     * Добавление товара в корзину
     *
     * @param $from
     * @param $id
     * @param int $quantity
     * @return string
     */
    public function actionIndex($from, $id, $quantity = 1)
    {
        $cart = CartModule::getInstance()->getCart();
        $status = 'ko';
        if ($cart->buy($from, $id, $quantity)) {
            $status = 'ok';
        }
        $result = [
            'status'        => $status,
            'totalQuantity' => $cart->totalQuantity,
            'totalAmount'   => $cart->totalAmount,
            'incart'        => $cart->hasItem($from, $id),
        ];
        return json_encode($result);
    }

    public function actionQuantity($from, $id, $quantity)
    {
        $cart = CartModule::getInstance()->getCart();
        $status = 'ko';
        if ($cart->setQuantity($from, $id, $quantity)) {
            $status = 'ok';
        }
        $result = [
            'status'        => $status,
            'totalQuantity' => $cart->totalQuantity,
            'totalAmount'   => $cart->totalAmount,
            'incart'        => $cart->hasItem($from, $id),
        ];
        return json_encode($result);
    }

    public function actionDelete($from, $id)
    {
        $cart = CartModule::getInstance()->getCart();
        $cart->delete($from, $id);

        $result = [
            'status'        => 'ok',
            'totalQuantity' => $cart->totalQuantity,
            'totalAmount'   => $cart->totalAmount,
            'incart'        => $cart->hasItem($from, $id),
        ];
        return json_encode($result);
    }
}