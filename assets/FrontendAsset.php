<?php
namespace gyrocoder\yii2\shopcart\assets;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FrontendAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/frontend';

    public $css = [
        'shopcart.css',
    ];

    public $js = [
        'shopcart.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}