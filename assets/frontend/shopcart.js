function updateCartStatus(r, from, id)
{
    $('[data-role="cart-total-quantity"]').text(r.totalQuantity);
    $('[data-role="cart-total-amount"]').text(r.totalAmount);
    if (r.incart) {
        var elemRole = 'gsc-' + from + '-' + id;
        $('[data-value="' + elemRole + '-quantity"]').val(r.incart.quantity);
        $('[data-value="' + elemRole + '-amount"]').text(r.incart.amount);
    }
}

function onPressButton(elem, increment)
{
    var container = $(elem).closest('[data-role="gsc-item"]');

    var from = container.attr('data-source');
    var id   = container.attr('data-id');
    var min  = container.attr('data-min');
    var max  = container.attr('data-max');
    var quantity = parseInt(container.find('[data-role="gsc-product-quantity"]').val());

    var action = $(elem).closest('[data-role="gsc-control-holder"]').attr('data-action');

    if ('recalculate' == action) {
        // пересчет реальной корзины
        quantity = quantity + increment;
        var url = container.attr('data-url') + '/default/quantity?from=' + from + '&id=' + id + '&quantity=' + quantity;

        $.ajax({
            dataType: "json",
            url: url,
            success: function(r) {
                updateCartStatus(r, from, id);
            }
        });

    } else {
        // только менфем количество
        quantity = quantity + increment;
        if(quantity<=max && quantity>=min) container.find('[data-role="gsc-product-quantity"]').val(quantity);
    }

    return false;
}

$(document).on('click', '[data-role="gsc-product-minus"]', function(){
    return onPressButton(this, -1);
});

$(document).on('click', '[data-role="gsc-product-plus"]', function(){
    return onPressButton(this, +1);
});

$(document).on('click', '[data-role="gsc-product-delete"]', function(){
    var container = $(this).closest('[data-role="gsc-item"]');
    var from = container.attr('data-source');
    var id   = container.attr('data-id');
    var url  = container.attr('data-url') + '/default/delete?from=' + from + '&id=' + id;

    $.ajax({
        dataType: "json",
        url: url,
        success: function(r) {
            updateCartStatus(r, from, id);
            var elemRole = 'gsc-' + from + '-' + id;
            $('[data-value="' + elemRole + '-quantity"]').val(1);
            $('[data-value="' + elemRole + '-amount"]').text(0);
            container.find('[data-filter="incart"]').addClass('hidden');
            container.find('[data-filter="notincart"]').removeClass('hidden');
        }
    });
});

$(document).on('click', '[data-role="gsc-product-purchase"]', function(){
    var container = $(this).closest('[data-role="gsc-item"]');
    var from = container.attr('data-source');
    var id   = container.attr('data-id');
    var quantity = parseInt(container.find('[data-role="gsc-product-quantity"]').val());

    var url  = container.attr('data-url') + '?from=' + from + '&id=' + id + '&quantity=' + quantity;
    $.ajax({
        dataType: "json",
        url: url,
        success: function(r) {
            updateCartStatus(r, from, id);
            container.find('[data-filter="incart"]').removeClass('hidden');
            container.find('[data-filter="notincart"]').addClass('hidden');
        }
    });
});